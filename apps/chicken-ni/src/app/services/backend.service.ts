import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {format, startOfWeek, subDays} from 'date-fns';
import {PlantPollen, PollenData} from '../models/pollen-data';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  private readonly BASE_URL = environment.production ? 'http://meteo.iasma.it' : '';
  private readonly DATE_FORMAT = `YYYYMMDD`;

  constructor(private http: HttpClient) { }


  getData(): Observable<PollenData> {

    const prevWeek = subDays(startOfWeek(new Date(), {weekStartsOn: 1}), 1);

    const params = new HttpParams({
      fromObject: {
        u: 'TREC',
        p: 'T35xC',
        f: 'file',
        dtin: format(prevWeek, this.DATE_FORMAT),
        dtfi: format(prevWeek, this.DATE_FORMAT),
        l: '1',
      }
    });

    // Response example
    // data;Settimana;Ambrosia;Conc;Artemisia;Conc;Betula;Conc;Corylus;Conc;Cupressaceae-Taxaceae;Conc;Graminaceae;Conc;Oleaceae;Conc;Ostrya;Conc;Urticaceae;Conc
    // 2021-05-30;21;0;NULLA;0;NULLA;0;NULLA;0;NULLA;3;MEDIA;51;ALTA;8;MEDIA;1;BASSA;61;MEDIA

    return this.http.get(`${this.BASE_URL}/cgi-bin/pollini/get_taxa_week`, {params, responseType: 'text'}).pipe(
      map<string, PollenData>(res => {
        const lines = res.split('\n');

        const [_dateH, _weekNoH, ...headers] = lines[0].split(';');
        const data = lines[1].split(';');

        const date = new Date(data[0]);
        const weekNo = parseInt(data[1], 10);

        const plants = headers.map<PlantPollen | null>((header, i) => {

          if (header === 'Conc') {
            return null;
          }

          return {
            name: header as any,
            concentrationValue: parseInt(data[i + 2]),
            concentrationType: data[i + 2 + 1] as any,
          };
        })
          .filter(p => !!p) as PlantPollen[];

        return {date, weekNo, plants};
      })
    )
  }

}
