import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {HomeRoutingModule} from './home.routing';
import {HomePageComponent} from './pages/home-page/home-page.component';

import {DataViewModule} from 'primeng/dataview';
import {CardModule} from 'primeng/card';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,

    DataViewModule,
    CardModule,
  ],
  declarations: [
    HomePageComponent
  ]
})
export class HomeModule { }
