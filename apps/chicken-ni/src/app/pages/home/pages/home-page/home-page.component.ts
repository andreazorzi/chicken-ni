import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {PollenData} from 'apps/chicken-ni/src/app/models/pollen-data';
import {CommonActions} from 'apps/chicken-ni/src/app/store/common/common.actions';
import {CommonState} from 'apps/chicken-ni/src/app/store/common/common.state';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePageComponent implements OnInit {


  @Select(CommonState.currentWeekData)
  currentWeekData$!: Observable<PollenData | null>;

  constructor(private store: Store) { }

  ngOnInit(): void {
    this.store.dispatch(new CommonActions.FetchCurrentData());
  }

}
