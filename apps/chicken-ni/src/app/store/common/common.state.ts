import {Injectable} from "@angular/core";
import {Action, Selector, State, StateContext} from "@ngxs/store";
import {tap} from "rxjs/operators";
import {PollenData} from "../../models/pollen-data";
import {BackendService} from "../../services/backend.service";
import {CommonActions} from "./common.actions";



export interface CommonStateModel {
  currentWeekData: PollenData | null;
}

@State<CommonStateModel>({
  name: 'common',
  defaults: {
    currentWeekData: null,
  }
})
@Injectable()
export class CommonState {

  constructor(private backend: BackendService) { }


  @Selector()
  static currentWeekData(state: CommonStateModel) {
    return state.currentWeekData;
  }

  @Action(CommonActions.FetchCurrentData)
  fetchCurrentData(ctx: StateContext<CommonStateModel>) {
    return this.backend.getData().pipe(
      tap(data => {
        ctx.patchState({currentWeekData: data});
      }),
    );
  }

}
