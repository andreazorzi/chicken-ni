import {NgModule} from '@angular/core';
import {NgxsModule} from '@ngxs/store';
import {NgxsReduxDevtoolsPluginModule} from '@ngxs/devtools-plugin';
import {environment} from '../../environments/environment';
import {CommonState} from './common/common.state';

@NgModule({
  imports: [
    NgxsModule.forRoot(
      [CommonState],
      {
        developmentMode: !environment.production
      }
    ),
    NgxsReduxDevtoolsPluginModule.forRoot(),
  ]
})
export class AppStoreModule { }
