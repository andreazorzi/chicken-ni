// eslint-disable-next-line @typescript-eslint/no-var-requires
const withNx = require('@nrwl/next/plugins/with-nx');
const { withLayer0, withServiceWorker } = require('@layer0/next/config')

/**
 * @type {import('@nrwl/next/plugins/with-nx').WithNxOptions}
 **/
const nextConfig = {
  nx: {
    // Set this to false if you do not want to use SVGR
    // See: https://github.com/gregberge/svgr
    svgr: true,
  },
  future: {
    webpack5: true,
  },
  // Output sourcemaps so that stacktraces have original source filenames and line numbers when tailing
  // the logs in the Layer0 developer console.
  layer0SourceMaps: true,

};

module.exports = withNx(withLayer0(withServiceWorker(nextConfig)));
