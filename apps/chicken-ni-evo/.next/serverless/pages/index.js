/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ 5057:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2322);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7729);
/* harmony import */ var primereact_toolbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2460);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2784);
/* harmony import */ var primereact_button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2911);




function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }











function CustomApp({
  Component,
  pageProps
}) {
  const leftContents = /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(primereact_button__WEBPACK_IMPORTED_MODULE_4__/* .Button */ .zx, {
    label: "Storico",
    className: "p-button-link"
  });

  const rightContents = '';
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_head__WEBPACK_IMPORTED_MODULE_1__.default, {
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("title", {
        children: "Pollini"
      })
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("header", {
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(primereact_toolbar__WEBPACK_IMPORTED_MODULE_2__/* .Toolbar */ .o, {
          left: leftContents,
          right: rightContents
        })
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("main", {
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Component, _objectSpread({}, pageProps))
      })]
    })]
  });
}

/* harmony default export */ __webpack_exports__["default"] = (CustomApp);

/***/ }),

/***/ 1487:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Index": function() { return /* binding */ Index; },
/* harmony export */   "getServerSideProps": function() { return /* binding */ getServerSideProps; }
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2322);
/* harmony import */ var _index_module_scss__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(9784);
/* harmony import */ var _index_module_scss__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_index_module_scss__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5155);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(date_fns__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var primereact_dataview__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9723);
/* harmony import */ var primereact_card__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(727);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2784);







const BASE_URL = 'http://meteo.iasma.it';
const DATE_FORMAT = `YYYYMMDD`;
class Index extends react__WEBPACK_IMPORTED_MODULE_4__.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      layout: 'grid'
    };
  }

  setProperty(value) {
    this.setState({
      layout: value
    });
  }

  render() {
    const itemTemplate = (plant, layout) => {
      if (this.state.layout === 'list') {
        return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "p-col-12 p-p-2",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(primereact_card__WEBPACK_IMPORTED_MODULE_3__/* .Card */ .Z, {
            title: plant.name,
            className: "p-card-shadow",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
              className: "p-d-flex p-ai-center p-jc-between",
              children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                children: ["Concentrazione ", plant.concentrationType]
              }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                children: plant.concentrationValue
              })]
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
              className: 'p-mt-2 ' + (_index_module_scss__WEBPACK_IMPORTED_MODULE_5___default().concentration) + ' ' + (_index_module_scss__WEBPACK_IMPORTED_MODULE_5___default())['concentration-' + plant.concentrationType]
            })]
          })
        });
      }

      if (layout === 'grid') {
        return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "p-col-12 p-md-4 p-p-2",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(primereact_card__WEBPACK_IMPORTED_MODULE_3__/* .Card */ .Z, {
            title: plant.name,
            className: "p-card-shadow",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
              className: "p-d-flex p-ai-center p-jc-between",
              children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                children: ["Concentrazione ", plant.concentrationType]
              }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                children: plant.concentrationValue
              })]
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
              className: 'p-mt-2 ' + (_index_module_scss__WEBPACK_IMPORTED_MODULE_5___default().concentration) + ' ' + (_index_module_scss__WEBPACK_IMPORTED_MODULE_5___default())['concentration-' + plant.concentrationType]
            })]
          })
        });
      }
    };

    const header = /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(primereact_dataview__WEBPACK_IMPORTED_MODULE_2__/* .DataViewLayoutOptions */ .uE, {
      layout: this.state.layout,
      onChange: e => this.setProperty(e.value)
    });

    return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: (_index_module_scss__WEBPACK_IMPORTED_MODULE_5___default().page),
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(primereact_dataview__WEBPACK_IMPORTED_MODULE_2__/* .DataView */ .VO, {
        header: header,
        value: this.props.data.plants,
        layout: this.state.layout,
        itemTemplate: itemTemplate
      })
    });
  }

}
/* harmony default export */ __webpack_exports__["default"] = (Index);
const getServerSideProps = async _context => {
  const prevWeek = (0,date_fns__WEBPACK_IMPORTED_MODULE_1__.subDays)((0,date_fns__WEBPACK_IMPORTED_MODULE_1__.startOfWeek)(new Date(), {
    weekStartsOn: 1
  }), 1);
  const url = new URL(`${BASE_URL}/cgi-bin/pollini/get_taxa_week`);
  const params = {
    u: 'TREC',
    p: 'T35xC',
    f: 'file',
    dtin: (0,date_fns__WEBPACK_IMPORTED_MODULE_1__.format)(prevWeek, DATE_FORMAT),
    dtfi: (0,date_fns__WEBPACK_IMPORTED_MODULE_1__.format)(prevWeek, DATE_FORMAT),
    l: '1'
  };
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
  const res = await fetch(url.toString());
  const payload = await res.text();
  const lines = payload.split('\n');
  const [_dateH, _weekNoH, ...headers] = lines[0].split(';');
  const data = lines[1].split(';');
  const date = new Date(data[0]).toISOString();
  const weekNo = parseInt(data[1], 10);
  const plants = headers.map((header, i) => {
    if (header === 'Conc') {
      return null;
    }

    return {
      name: header,
      concentrationValue: parseInt(data[i + 2]),
      concentrationType: data[i + 2 + 1]
    };
  }).filter(p => !!p);
  const dataRes = {
    date,
    weekNo,
    plants
  };
  return {
    props: {
      data: dataRes
    }
  };
};

/***/ }),

/***/ 7476:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "_app": function() { return /* binding */ _app; },
  "config": function() { return /* binding */ config; },
  "default": function() { return /* binding */ next_serverless_loaderpage_2F_absolutePagePath_private_next_pages_2Findex_tsx_absoluteAppPath_private_next_pages_2F_app_tsx_absoluteDocumentPath_next_2Fdist_2Fpages_2F_document_absoluteErrorPath_next_2Fdist_2Fpages_2F_error_absolute404Path_distDir_private_dot_next_buildId_EWMJjPYFn6G857mKavLBV_assetPrefix_generateEtags_true_poweredByHeader_true_canonicalBase_basePath_runtimeConfig_previewProps_7B_22previewModeId_22_3A_227c05995dde1636e406dee917c342b1e2_22_2C_22previewModeSigningKey_22_3A_2272e9d55c283b16229327c4a8953cbdff12b67c5ffcb834c716d9b0c4f404e29e_22_2C_22previewModeEncryptionKey_22_3A_2239febdd5f5af95b55520ece7a18131b58251878f472db954ccdc33dde31e14a5_22_7D_loadedEnvFiles_W10_3D_i18n_; },
  "getServerSideProps": function() { return /* binding */ getServerSideProps; },
  "getStaticPaths": function() { return /* binding */ getStaticPaths; },
  "getStaticProps": function() { return /* binding */ getStaticProps; },
  "render": function() { return /* binding */ render; },
  "renderReqToHTML": function() { return /* binding */ renderReqToHTML; },
  "unstable_getServerProps": function() { return /* binding */ unstable_getServerProps; },
  "unstable_getStaticParams": function() { return /* binding */ unstable_getStaticParams; },
  "unstable_getStaticPaths": function() { return /* binding */ unstable_getStaticPaths; },
  "unstable_getStaticProps": function() { return /* binding */ unstable_getStaticProps; }
});

// EXTERNAL MODULE: ../../node_modules/next/dist/next-server/server/node-polyfill-fetch.js
var node_polyfill_fetch = __webpack_require__(9407);
;// CONCATENATED MODULE: ./.next/routes-manifest.json
var routes_manifest_namespaceObject = {"Dg":[]};
;// CONCATENATED MODULE: ./.next/build-manifest.json
var build_manifest_namespaceObject = JSON.parse('{"polyfillFiles":["static/chunks/polyfills-a9aae5dc02dbfa55f2f9.js"],"devFiles":[],"ampDevFiles":[],"lowPriorityFiles":["static/EWMJjPYFn6G857mKavLBV/_buildManifest.js","static/EWMJjPYFn6G857mKavLBV/_ssgManifest.js"],"pages":{"/":["static/chunks/webpack-2d99c0a10319bd53ec24.js","static/chunks/framework-881ea4035a8b741d9304.js","static/chunks/22-fd9c899405a78312765b.js","static/chunks/595-908c49d619bb96696dd4.js","static/chunks/main-2975675edaa1a57c6ffb.js","static/chunks/731-d815615953f0e45ab617.js","static/chunks/255-fec18abc5c18588acbf5.js","static/css/7cc69e0f47a50cab822d.css","static/chunks/pages/index-e707c3c4b399970d648c.js"],"/_app":["static/chunks/webpack-2d99c0a10319bd53ec24.js","static/chunks/framework-881ea4035a8b741d9304.js","static/chunks/22-fd9c899405a78312765b.js","static/chunks/595-908c49d619bb96696dd4.js","static/chunks/main-2975675edaa1a57c6ffb.js","static/css/2c0ab75c68753bb90b06.css","static/chunks/731-d815615953f0e45ab617.js","static/css/0b96de82aa40dce16fac.css","static/chunks/pages/_app-534c39353f43347c2398.js"],"/_error":["static/chunks/webpack-2d99c0a10319bd53ec24.js","static/chunks/framework-881ea4035a8b741d9304.js","static/chunks/22-fd9c899405a78312765b.js","static/chunks/595-908c49d619bb96696dd4.js","static/chunks/main-2975675edaa1a57c6ffb.js","static/chunks/pages/_error-e7eedbbdaca8c383346a.js"]},"ampFirstPages":[]}');
;// CONCATENATED MODULE: ./.next/react-loadable-manifest.json
var react_loadable_manifest_namespaceObject = {};
// EXTERNAL MODULE: ../../node_modules/next/dist/build/webpack/loaders/next-serverless-loader/page-handler.js
var page_handler = __webpack_require__(7194);
;// CONCATENATED MODULE: ../../node_modules/next/dist/build/webpack/loaders/next-serverless-loader/index.js?page=%2F&absolutePagePath=private-next-pages%2Findex.tsx&absoluteAppPath=private-next-pages%2F_app.tsx&absoluteDocumentPath=next%2Fdist%2Fpages%2F_document&absoluteErrorPath=next%2Fdist%2Fpages%2F_error&absolute404Path=&distDir=private-dot-next&buildId=EWMJjPYFn6G857mKavLBV&assetPrefix=&generateEtags=true&poweredByHeader=true&canonicalBase=&basePath=&runtimeConfig=&previewProps=%7B%22previewModeId%22%3A%227c05995dde1636e406dee917c342b1e2%22%2C%22previewModeSigningKey%22%3A%2272e9d55c283b16229327c4a8953cbdff12b67c5ffcb834c716d9b0c4f404e29e%22%2C%22previewModeEncryptionKey%22%3A%2239febdd5f5af95b55520ece7a18131b58251878f472db954ccdc33dde31e14a5%22%7D&loadedEnvFiles=W10%3D&i18n=!

      
      
      
      

      
      const { processEnv } = __webpack_require__(782)
      processEnv([])
    
      
      const runtimeConfig = {}
      ;

      const documentModule = __webpack_require__(381)

      const appMod = __webpack_require__(5057)
      let App = appMod.default || appMod.then && appMod.then(mod => mod.default);

      const compMod = __webpack_require__(1487)

      const Component = compMod.default || compMod.then && compMod.then(mod => mod.default)
      /* harmony default export */ var next_serverless_loaderpage_2F_absolutePagePath_private_next_pages_2Findex_tsx_absoluteAppPath_private_next_pages_2F_app_tsx_absoluteDocumentPath_next_2Fdist_2Fpages_2F_document_absoluteErrorPath_next_2Fdist_2Fpages_2F_error_absolute404Path_distDir_private_dot_next_buildId_EWMJjPYFn6G857mKavLBV_assetPrefix_generateEtags_true_poweredByHeader_true_canonicalBase_basePath_runtimeConfig_previewProps_7B_22previewModeId_22_3A_227c05995dde1636e406dee917c342b1e2_22_2C_22previewModeSigningKey_22_3A_2272e9d55c283b16229327c4a8953cbdff12b67c5ffcb834c716d9b0c4f404e29e_22_2C_22previewModeEncryptionKey_22_3A_2239febdd5f5af95b55520ece7a18131b58251878f472db954ccdc33dde31e14a5_22_7D_loadedEnvFiles_W10_3D_i18n_ = (Component);
      const getStaticProps = compMod['getStaticProp' + 's'] || compMod.then && compMod.then(mod => mod['getStaticProp' + 's'])
      const getStaticPaths = compMod['getStaticPath' + 's'] || compMod.then && compMod.then(mod => mod['getStaticPath' + 's'])
      const getServerSideProps = compMod['getServerSideProp' + 's'] || compMod.then && compMod.then(mod => mod['getServerSideProp' + 's'])

      // kept for detecting legacy exports
      const unstable_getStaticParams = compMod['unstable_getStaticParam' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getStaticParam' + 's'])
      const unstable_getStaticProps = compMod['unstable_getStaticProp' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getStaticProp' + 's'])
      const unstable_getStaticPaths = compMod['unstable_getStaticPath' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getStaticPath' + 's'])
      const unstable_getServerProps = compMod['unstable_getServerProp' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getServerProp' + 's'])

      let config = compMod['confi' + 'g'] || (compMod.then && compMod.then(mod => mod['confi' + 'g'])) || {}
      const _app = App

      const combinedRewrites = Array.isArray(routes_manifest_namespaceObject.Dg)
        ? routes_manifest_namespaceObject.Dg
        : []

      if (!Array.isArray(routes_manifest_namespaceObject.Dg)) {
        combinedRewrites.push(...routes_manifest_namespaceObject.Dg.beforeFiles)
        combinedRewrites.push(...routes_manifest_namespaceObject.Dg.afterFiles)
        combinedRewrites.push(...routes_manifest_namespaceObject.Dg.fallback)
      }

      const { renderReqToHTML, render } = (0,page_handler/* getPageHandler */.u)({
        pageModule: compMod,
        pageComponent: Component,
        pageConfig: config,
        appModule: App,
        documentModule: documentModule,
        errorModule: __webpack_require__(4576),
        notFoundModule: undefined,
        pageGetStaticProps: getStaticProps,
        pageGetStaticPaths: getStaticPaths,
        pageGetServerSideProps: getServerSideProps,

        assetPrefix: "",
        canonicalBase: "",
        generateEtags: true,
        poweredByHeader: true,

        runtimeConfig,
        buildManifest: build_manifest_namespaceObject,
        reactLoadableManifest: react_loadable_manifest_namespaceObject,

        rewrites: combinedRewrites,
        i18n: undefined,
        page: "/",
        buildId: "EWMJjPYFn6G857mKavLBV",
        escapedBuildId: "EWMJjPYFn6G857mKavLBV",
        basePath: "",
        pageIsDynamic: false,
        encodedPreviewProps: {previewModeId:"7c05995dde1636e406dee917c342b1e2",previewModeSigningKey:"72e9d55c283b16229327c4a8953cbdff12b67c5ffcb834c716d9b0c4f404e29e",previewModeEncryptionKey:"39febdd5f5af95b55520ece7a18131b58251878f472db954ccdc33dde31e14a5"}
      })
      
    

/***/ }),

/***/ 9784:
/***/ (function(module) {

// Exports
module.exports = {
	"concentration": "index_concentration__2tKTR",
	"concentration-NULLA": "index_concentration-NULLA__3Ii2o",
	"concentration-BASSA": "index_concentration-BASSA__n7red",
	"concentration-MEDIA": "index_concentration-MEDIA__1m5q-",
	"concentration-ALTA": "index_concentration-ALTA__3vLr4"
};


/***/ }),

/***/ 4526:
/***/ (function(module) {

function webpackEmptyContext(req) {
	var e = new Error("Cannot find module '" + req + "'");
	e.code = 'MODULE_NOT_FOUND';
	throw e;
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
webpackEmptyContext.id = 4526;
module.exports = webpackEmptyContext;

/***/ }),

/***/ 4293:
/***/ (function(module) {

"use strict";
module.exports = require("buffer");;

/***/ }),

/***/ 5532:
/***/ (function(module) {

"use strict";
module.exports = require("critters");;

/***/ }),

/***/ 6417:
/***/ (function(module) {

"use strict";
module.exports = require("crypto");;

/***/ }),

/***/ 8614:
/***/ (function(module) {

"use strict";
module.exports = require("events");;

/***/ }),

/***/ 5747:
/***/ (function(module) {

"use strict";
module.exports = require("fs");;

/***/ }),

/***/ 8605:
/***/ (function(module) {

"use strict";
module.exports = require("http");;

/***/ }),

/***/ 7211:
/***/ (function(module) {

"use strict";
module.exports = require("https");;

/***/ }),

/***/ 3700:
/***/ (function(module) {

"use strict";
module.exports = require("next/dist/compiled/@ampproject/toolbox-optimizer");;

/***/ }),

/***/ 2087:
/***/ (function(module) {

"use strict";
module.exports = require("os");;

/***/ }),

/***/ 5622:
/***/ (function(module) {

"use strict";
module.exports = require("path");;

/***/ }),

/***/ 1191:
/***/ (function(module) {

"use strict";
module.exports = require("querystring");;

/***/ }),

/***/ 2413:
/***/ (function(module) {

"use strict";
module.exports = require("stream");;

/***/ }),

/***/ 4304:
/***/ (function(module) {

"use strict";
module.exports = require("string_decoder");;

/***/ }),

/***/ 8835:
/***/ (function(module) {

"use strict";
module.exports = require("url");;

/***/ }),

/***/ 1669:
/***/ (function(module) {

"use strict";
module.exports = require("util");;

/***/ }),

/***/ 8761:
/***/ (function(module) {

"use strict";
module.exports = require("zlib");;

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete __webpack_module_cache__[moduleId];
/******/ 		}
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/******/ 	// the startup function
/******/ 	__webpack_require__.x = function() {
/******/ 		// Load entry module and return exports
/******/ 		// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 		var __webpack_exports__ = __webpack_require__.O(undefined, [795], function() { return __webpack_require__(7476); })
/******/ 		__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 		return __webpack_exports__;
/******/ 	};
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	!function() {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = function(result, chunkIds, fn, priority) {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var chunkIds = deferred[i][0];
/******/ 				var fn = deferred[i][1];
/******/ 				var priority = deferred[i][2];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every(function(key) { return __webpack_require__.O[key](chunkIds[j]); })) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					result = fn();
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/ensure chunk */
/******/ 	!function() {
/******/ 		__webpack_require__.f = {};
/******/ 		// This file contains only the entry chunk.
/******/ 		// The chunk loading function for additional chunks
/******/ 		__webpack_require__.e = function(chunkId) {
/******/ 			return Promise.all(Object.keys(__webpack_require__.f).reduce(function(promises, key) {
/******/ 				__webpack_require__.f[key](chunkId, promises);
/******/ 				return promises;
/******/ 			}, []));
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/get javascript chunk filename */
/******/ 	!function() {
/******/ 		// This function allow to reference async chunks and sibling chunks for the entrypoint
/******/ 		__webpack_require__.u = function(chunkId) {
/******/ 			// return url for filenames based on template
/******/ 			return "../" + "webpack-runtime-commons" + ".js";
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	!function() {
/******/ 		__webpack_require__.nmd = function(module) {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/compat */
/******/ 	
/******/ 	                // Font manifest declaration
/******/ 	                __webpack_require__.__NEXT_FONT_MANIFEST__ = [];
/******/ 	            // Enable feature:
/******/ 	            process.env.__NEXT_OPTIMIZE_FONTS = JSON.stringify(true);/* webpack/runtime/require chunk loading */
/******/ 	!function() {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded chunks
/******/ 		// "1" means "loaded", otherwise not loaded yet
/******/ 		var installedChunks = {
/******/ 			405: 1
/******/ 		};
/******/ 		
/******/ 		__webpack_require__.O.require = function(chunkId) { return installedChunks[chunkId]; };
/******/ 		
/******/ 		var installChunk = function(chunk) {
/******/ 			var moreModules = chunk.modules, chunkIds = chunk.ids, runtime = chunk.runtime;
/******/ 			for(var moduleId in moreModules) {
/******/ 				if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 					__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 				}
/******/ 			}
/******/ 			if(runtime) runtime(__webpack_require__);
/******/ 			for(var i = 0; i < chunkIds.length; i++)
/******/ 				installedChunks[chunkIds[i]] = 1;
/******/ 			__webpack_require__.O();
/******/ 		};
/******/ 		
/******/ 		// require() chunk loading for javascript
/******/ 		__webpack_require__.f.require = function(chunkId, promises) {
/******/ 			// "1" is the signal for "already loaded"
/******/ 			if(!installedChunks[chunkId]) {
/******/ 				if(true) { // all chunks have JS
/******/ 					installChunk(require("../chunks/" + __webpack_require__.u(chunkId)));
/******/ 				} else installedChunks[chunkId] = 1;
/******/ 			}
/******/ 		};
/******/ 		
/******/ 		// no external install chunk
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/startup chunk dependencies */
/******/ 	!function() {
/******/ 		var next = __webpack_require__.x;
/******/ 		__webpack_require__.x = function() {
/******/ 			__webpack_require__.e(795);
/******/ 			return next();
/******/ 		};
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// run startup
/******/ 	var __webpack_exports__ = __webpack_require__.x();
/******/ 	module.exports = __webpack_exports__;
/******/ 	
/******/ })()
;
//# sourceMappingURL=index.js.map