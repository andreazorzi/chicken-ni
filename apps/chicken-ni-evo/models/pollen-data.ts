

export interface PlantPollen {
  name: 'Ambrosia' | 'Artemisia' | 'Betula' | 'Corylus' | 'Cupressaceae-Taxaceae' | 'Graminaceae' | 'Oleaceae' | 'Ostrya' | 'Urticaceae';
  concentrationValue: number;
  concentrationType: 'NULLA' | 'BASSA' | 'MEDIA' | 'ALTA';
}

export interface PollenData {

  date: string;
  weekNo: number;

  plants: PlantPollen[];

}
