// This file was automatically added by layer0 deploy.
// You should commit this file to source control.

const { Router } = require('@layer0/core/router')
const { nextRoutes } = require('@layer0/next')

module.exports = new Router().use(nextRoutes)
