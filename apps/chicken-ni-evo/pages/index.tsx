import styles from './index.module.scss';
import { GetServerSideProps } from 'next';
import { subDays, startOfWeek, format } from 'date-fns';
import { PlantPollen, PollenData } from '../models/pollen-data';
import { DataView, DataViewLayoutOptions } from 'primereact/dataview';
import { Card } from 'primereact/card';
import React from 'react';

const BASE_URL = 'http://meteo.iasma.it';
const DATE_FORMAT = `YYYYMMDD`;

export class Index extends React.PureComponent<
  { data: PollenData },
  { layout: 'list' | 'grid' }
> {
  constructor(props: { data: PollenData }) {
    super(props);
    this.state = { layout: 'grid' };
  }

  setProperty(value: 'list' | 'grid') {
    this.setState({
      layout: value,
    });
  }

  render() {
    const itemTemplate = (plant: PlantPollen, layout: 'list' | 'grid') => {
      if (this.state.layout === 'list') {
        return (
          <div className="p-col-12 p-p-2">
            <Card title={plant.name} className="p-card-shadow">
              <div className="p-d-flex p-ai-center p-jc-between">
                <div>Concentrazione {plant.concentrationType}</div>
                <div>{plant.concentrationValue}</div>
              </div>
              <div
                className={
                  'p-mt-2 ' +
                  styles.concentration +
                  ' ' +
                  styles['concentration-' + plant.concentrationType]
                }
              ></div>
            </Card>
          </div>
        );
      }

      if (layout === 'grid') {
        return (
          <div className="p-col-12 p-md-4 p-p-2">
            <Card title={plant.name} className="p-card-shadow">
              <div className="p-d-flex p-ai-center p-jc-between">
                <div>Concentrazione {plant.concentrationType}</div>
                <div>{plant.concentrationValue}</div>
              </div>
              <div
                className={
                  'p-mt-2 ' +
                  styles.concentration +
                  ' ' +
                  styles['concentration-' + plant.concentrationType]
                }
              ></div>
            </Card>
          </div>
        );
      }
    };

    const header = (
      <DataViewLayoutOptions
        layout={this.state.layout}
        onChange={(e) => this.setProperty(e.value as any)}
      />
    );

    return (
      <div className={styles.page}>
        <DataView
          header={header}
          value={this.props.data.plants}
          layout={this.state.layout}
          itemTemplate={itemTemplate}
        ></DataView>
      </div>
    );
  }
}

export default Index;

export const getServerSideProps: GetServerSideProps = async (_context) => {
  const prevWeek = subDays(startOfWeek(new Date(), { weekStartsOn: 1 }), 1);

  const url = new URL(`${BASE_URL}/cgi-bin/pollini/get_taxa_week`);
  const params: { [key: string]: string } = {
    u: 'TREC',
    p: 'T35xC',
    f: 'file',
    dtin: format(prevWeek, DATE_FORMAT),
    dtfi: format(prevWeek, DATE_FORMAT),
    l: '1',
  };
  Object.keys(params).forEach((key) =>
    url.searchParams.append(key, params[key])
  );

  const res = await fetch(url.toString());
  const payload = await res.text();

  const lines = payload.split('\n');

  const [_dateH, _weekNoH, ...headers] = lines[0].split(';');
  const data = lines[1].split(';');

  const date = new Date(data[0]).toISOString();
  const weekNo = parseInt(data[1], 10);

  const plants = headers
    .map<PlantPollen | null>((header, i) => {
      if (header === 'Conc') {
        return null;
      }

      return {
        name: header as any,
        concentrationValue: parseInt(data[i + 2]),
        concentrationType: data[i + 2 + 1] as any,
      };
    })
    .filter((p) => !!p) as PlantPollen[];

  const dataRes = { date, weekNo, plants };

  return {
    props: {
      data: dataRes,
    },
  };
};
