import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import './styles.css';

import { AppProps } from 'next/app';
import Head from 'next/head';
import { Toolbar } from 'primereact/toolbar';
import React from 'react';
import { Button } from 'primereact/button';

function CustomApp({ Component, pageProps }: AppProps) {
  const leftContents = <Button label="Storico" className="p-button-link" />;

  const rightContents = '';

  return (
    <>
      <Head>
        <title>Pollini</title>
      </Head>
      <div>
        <header>
          <Toolbar left={leftContents} right={rightContents} />
        </header>
        <main>
          <Component {...pageProps} />
        </main>
      </div>
    </>
  );
}

export default CustomApp;
